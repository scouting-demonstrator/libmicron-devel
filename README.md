# libmicron-devel

Library to allow compilation against micron libs

Currently compiled against picocomputing-2022.1.3 on slc7

Can make with makelib.sh if picocomputing is present under variable PICO_BASE e.g PICO_BASE=/usr/src/picocomputing-2022.1.3/
