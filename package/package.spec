%define _prefix  /opt/libmicron-devel
%define _systemd /etc/systemd/system
%define _homedir %{_prefix}/libmicron-devel
%define debug_package %{nil}

# To skip bytecompilation
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

Name: cms-libmicron-devel
Version: %{_version}
Release: %{_release}
Summary: CMS L1 Scouting micron library wrapper
Group: CMS/L1Scouting
License: GPL
Vendor: CMS/L1Scouting
Packager: %{_packager}
Source: %{name}.tar
ExclusiveOs: linux
Provides: cms-libmicron-devel
Requires: openssh

Prefix: %{_prefix}

%description
CMS L1 Scouting micron library wrapper

%files
%defattr(-,scouter,root,-)
%attr(-, scouter, root) %dir %{_prefix}
%attr(-, scouter, root) %dir %{_prefix}/logs
%attr(-, scouter, root) %dir %{_homedir}
%attr(-, scouter, root) %{_homedir}/*

%prep
%setup -c

%build

%install
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_prefix}
pwd
tar cf - . | (cd  $RPM_BUILD_ROOT%{_prefix}; tar xfp - )
mkdir -p $RPM_BUILD_ROOT%{_systemd}
mkdir -p $RPM_BUILD_ROOT%{_prefix}/logs


%clean
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT || :

%pre

%post
cp $RPM_BUILD_ROOT%{_prefix}/libmicron-devel/src/micron_dma.h /usr/include/
cp $RPM_BUILD_ROOT%{_prefix}/libmicron-devel/src/libmicron.a /usr/lib/

%preun

%postun

# Only for uninstall!
if [ $1 -eq 0 ] ; then

  # Removing folder
  rm -fR %{_homedir}
  rmdir --ignore-fail-on-non-empty %{_prefix}/logs
  rmdir --ignore-fail-on-non-empty %{_prefix}

fi
